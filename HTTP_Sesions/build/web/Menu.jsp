<%-- 
    Document   : Menu
    Created on : Feb 8, 2024, 7:21:34 PM
    Author     : Bairon
--%>

<%@page import="Modelo.Privilegio"%>
<%@page import="Basedatos.Data"%>
<%@page import="Modelo.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu Principal</title>
    </head>
    <body>
        <h1>Menu Principal</h1>
     
           <%
         Data d = new Data();
         Usuario u = (Usuario)request.getSession().getAttribute("Session");         
         if(u == null ){
         response.sendRedirect("Error.jsp");
            } else {
         out.println("Bienvenido senor/a: "+ u.getName());
         out.println("<hr>");
         out.println("<Identificacion>"+ u.getDi());
         out.println("<br/>");
         out.println("<Nombre>"+ u.getName());
         out.println("<br/>");
         
         Privilegio p = d.getPrivilegio(u.getPrivilegio());
         
         out.println("<Privilegio>"+ p.getDescripcion());
         out.println("<hr/>");

         switch(p.getId()){
         case 1:{
         out.println("Usuario Administrador");
         break;    
         }
         case 2:{
         out.println("Usuario Estandard");
         break;     
         }
         case 3:{
         out.println("Usuario Operador");
         break;
         }
         }
            }        
         
          %>
    
     <hr/>
     <%--
     Menu en construccion, click <a href="index.jsp">aca</a>
        para volver.
     Usuario u =  (Usuario)session.getAttribute("Usuario");
    --%>
     
     <a href="CerrarSesion"> Cerrar Sesion <a/>       
    </body>
</html>
