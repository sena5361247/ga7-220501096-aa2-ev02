/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Basedatos;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bairon
 */
public class Conexion {
    String bd ="bd_sesiones";
    String url ="jdbc:mysql://localhost:3306/";
    String user ="root";
    String password ="1234";
    String driver ="com.mysql.cj.jdbc.Driver";
    Connection con;
    Statement sen;
    ResultSet rs;

    public Conexion() {        
        
    }
    
    public Connection conectar (){
          try {
              Class.forName(driver);
              con= DriverManager.getConnection(url+bd,user,password);
              System.out.println("Se conecto a BD "+bd);
          } catch (ClassNotFoundException |SQLException ex) {
              System.out.println("No se conecto a BD "+bd);
              Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
          }
          return con;
    }
           
    public void ejecutar (String query) throws SQLException  {
    sen = con.createStatement();
    sen.executeUpdate(query);
    sen.close();
}

    public  ResultSet ejecutarSelect(String query) throws SQLException{
    sen = con.createStatement();
    rs = sen.executeQuery(query);
    return rs;
    }

    public void desconectar () throws SQLException {
     con.close();
    }
    
//public static void main (String[] args) throws ClassNotFoundException, SQLException{
// Conexion c = new Conexion ();
// c.conectar(); 
//}
}


    
    
    
    
    
    
    
    
    
    
    
    
