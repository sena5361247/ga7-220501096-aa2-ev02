/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bairon
 */
public class Usuario {
    private String mail;
    private String pass;
    private String name;
    private List<Error>errors;
    private int di;
    private int privilegio;
        
    public int getDi() {
        return di;
    }

    public void setDi(int di) {
        this.di = di;
    }

    public int getPrivilegio() {
        return privilegio;
    }

    public void setPrivilegio(int privilegio) {
        this.privilegio = privilegio;
    }

    
    public Usuario(String mail, String pass) {
        this.mail = mail;
        this.pass = pass;
        errors= new ArrayList<Error>();
                    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void addError (Error error){
    errors.add(error);
    }
    
    public boolean errorExiste(){
    return !errors.isEmpty();
    }

    public List<Error> getErrors() {
        return errors;
    }
    
}
